package com.chanchal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphExerciseApplication.class, args);
	}

}
