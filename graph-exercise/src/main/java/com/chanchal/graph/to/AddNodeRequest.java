package com.chanchal.graph.to;

import com.chanchal.graph.enums.Level;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddNodeRequest {
	private Long parentId;
	private Long childId;
	private Level level;
	private Integer weight;
}
