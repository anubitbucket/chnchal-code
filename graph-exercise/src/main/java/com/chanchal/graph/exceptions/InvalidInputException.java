package com.chanchal.graph.exceptions;

public class InvalidInputException extends RuntimeException {

	private static final long serialVersionUID = 3087152028780682866L;

	public InvalidInputException(String message) {
		super(message);
	}

	
}
