package com.chanchal.graph.enums;

public enum Level {
	LEVEL_1(1), LEVEL_2(2), LEVEL_3(3);

	private int value;

	private Level(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public boolean isParent(Level otherLevel) {
		return otherLevel.ordinal() - this.ordinal() == 1;
	}

}
