package com.chanchal.graph.enums;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.chanchal.graph.models.Node;

public enum Tree {
	INVESTOR;
	private Map<Long, Node> nodes = new ConcurrentHashMap<>();

	public void addNode(Node node) {
		nodes.putIfAbsent(node.getId(), node);
	}
	
	public Node getNodeById(long nodeId) {
		return nodes.get(nodeId);
	}
}
