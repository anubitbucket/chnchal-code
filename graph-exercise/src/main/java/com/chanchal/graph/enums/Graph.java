package com.chanchal.graph.enums;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum Graph {
	INVESTOR;
	private Map<Long, List<Long>> context = new ConcurrentHashMap<>();

	public Map<Long, List<Long>> getContext() {
		return context;
	}
}
