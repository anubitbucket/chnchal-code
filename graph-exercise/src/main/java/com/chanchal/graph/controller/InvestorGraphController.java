package com.chanchal.graph.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.chanchal.graph.models.Node;
import com.chanchal.graph.service.GraphReader;
import com.chanchal.graph.service.GraphWriter;
import com.chanchal.graph.to.AddNodeRequest;
import com.chanchal.graph.to.Response;
import com.chanchal.graph.validator.RequestValidator;

@RestController
public class InvestorGraphController {

	@Autowired
	private GraphReader graphReader;

	@Autowired
	private GraphWriter graphWriter;

	@Autowired
	private RequestValidator requestValidator;

	/**
	 * API for registering nodes
	 */
	@PostMapping("/nodes")
	public Node createNode(@RequestBody Node request) {
		requestValidator.validateCreateNodeRequest(request);
		return graphWriter.createNode(request.getId(), request.getName(), request.getLevel(), request.getValue());
	}

	/**
	 * api introduced for testing
	 */
	@PostMapping("/nodes/list")
	public void createNode(@RequestBody List<Node> requestList) {
		requestList.stream().forEach(request -> graphWriter.createNode(request.getId(), request.getName(),
				request.getLevel(), request.getValue()));
	}

	/**
	 * API for creating adding nodes and edges to the Investors Graph.
	 */
	@PostMapping("graph/nodes")
	public Response addNodeToGraph(@RequestBody AddNodeRequest request) {
		requestValidator.validateAddNodeRequest(request);
		return graphWriter.addNode(request.getParentId(), request.getChildId(), request.getLevel(), request.getWeight());
	}

	/**
	 * API for retrieving market value against a InvestorId or fundId
	 */
	@GetMapping("graph/nodes/{id}/value")
	public Double getMarketValue(@PathVariable("id") Long id, @RequestParam(value = "exclusionIds", required = false) List<Long> exclusionList) {
		return graphReader.getMarketValue(id, exclusionList);
	}
}
