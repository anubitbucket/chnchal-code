package com.chanchal.graph.validator;

import java.util.Objects;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.chanchal.graph.enums.Level;
import com.chanchal.graph.exceptions.InvalidInputException;
import com.chanchal.graph.models.Node;
import com.chanchal.graph.to.AddNodeRequest;

@Component
public class RequestValidator {

	public void validateAddNodeRequest(AddNodeRequest request) {
		validateLongField(request.getParentId(), "Invalid parentId Passed");
		validateLongField(request.getChildId(), "Invalid childId Passed");
	}

	public void validateCreateNodeRequest(Node request) {
		validateLongField(request.getId(), "Invalid id Passed");
		if (StringUtils.isEmpty(request.getName())) {
			throw new InvalidInputException("Invalid name passed");
		}
		if (Objects.isNull(request.getLevel())) {
			throw new InvalidInputException("Invalid level passed");
		}
	}

	private void validateLongField(Long id, String message) {
		if (Objects.isNull(id) || id < 1L) {
			throw new InvalidInputException(message);
		}
	}

	public void validateLevelsForParentChild(Level parentLevel, Level childLevel) {
		if (childLevel != null && !parentLevel.isParent(childLevel)) {
			throw new InvalidInputException("invalid parent and child level passed");
		}
	}

	public void validateWeightForLevel3Child(Level childLevel, Integer weight) {
		if (Level.LEVEL_3 == childLevel && (Objects.isNull(weight) || weight <= 0)) {
			throw new InvalidInputException("Weight not provided or invalid value provided for level 3 child");
		}
	}
}
