package com.chanchal.graph.service;

import java.util.List;

public interface GraphReader {

	double getMarketValue(long nodeId, List<Long> exclusionList);
}
