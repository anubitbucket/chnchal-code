package com.chanchal.graph.service;

import com.chanchal.graph.enums.Level;
import com.chanchal.graph.models.Node;
import com.chanchal.graph.to.Response;

public interface GraphWriter {
	Response addNode(Long parentId, Long childId, Level level, Integer weight);

	Node createNode(Long id, String nodeName, Level level, Float value);
}
