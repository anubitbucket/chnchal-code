package com.chanchal.graph.service.impl;

import static com.chanchal.graph.enums.Graph.INVESTOR;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.chanchal.graph.enums.Level;
import com.chanchal.graph.enums.Tree;
import com.chanchal.graph.exceptions.InvalidInputException;
import com.chanchal.graph.models.Node;
import com.chanchal.graph.service.GraphReader;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GraphReaderImpl implements GraphReader{

	@Override
	public double getMarketValue(long nodeId, List<Long> exclusionList) {
		log.info("retrieving market value for Node:{}", nodeId);
		Map<Long, List<Long>> investorsGraph = INVESTOR.getContext();
		Node node = Tree.INVESTOR.getNodeById(nodeId);
		if (Objects.isNull(node)) {
			throw new InvalidInputException("Invalid nodeId passed");
		}
		Level nodeLevel = node.getLevel();
		List<Long> holdingIds;
		switch (nodeLevel) {
		case LEVEL_1:
			List<Long> fundIds = investorsGraph.get(nodeId);
			holdingIds = fundIds.stream().flatMap(f -> investorsGraph.get(f).stream()).collect(Collectors.toList());
			return getMarketValueForHoldings(holdingIds, exclusionList);
		case LEVEL_2:
			holdingIds = investorsGraph.get(nodeId);
			return getMarketValueForHoldings(holdingIds, exclusionList);
		default:
			return node.getValue();
		}
	}

	private double getMarketValueForHoldings(List<Long> holdingIds, List<Long> exclusionList) {
		holdingIds.removeAll(exclusionList);
		return holdingIds.stream().mapToDouble(f -> Tree.INVESTOR.getNodeById(f).getValue()).sum();
	}
}
