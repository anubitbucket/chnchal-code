package com.chanchal.graph.service.impl;

import static com.chanchal.graph.enums.Graph.INVESTOR;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chanchal.graph.enums.Level;
import com.chanchal.graph.enums.Tree;
import com.chanchal.graph.exceptions.InvalidInputException;
import com.chanchal.graph.models.Node;
import com.chanchal.graph.service.GraphWriter;
import com.chanchal.graph.to.Response;
import com.chanchal.graph.validator.RequestValidator;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GraphWriterImpl implements GraphWriter{

	@Autowired
	private RequestValidator requestValidator;

	@Override
	public Response addNode(Long parentId, Long childId, Level level, Integer weight) {
		log.info("Adding Node to Graph");
		Node parent, child;
		if (Objects.isNull(parent = Tree.INVESTOR.getNodeById(parentId))) {
			parent = createNode(parentId, "name:" + parentId, level, null);
		}
		if (Objects.isNull(child = Tree.INVESTOR.getNodeById(childId))) {
			child = createNode(childId, "name:" + childId, null, null);
		}
		requestValidator.validateLevelsForParentChild(parent.getLevel(), child.getLevel());
		requestValidator.validateWeightForLevel3Child(child.getLevel(), weight);
		Tree.INVESTOR.addNode(parent);
		Tree.INVESTOR.addNode(child);
		if (child.getLevel() == Level.LEVEL_3) {
			addNodeToGraph(parentId, childId, weight, child.getLevel());
		} else {
			addNodeToGraph(parentId, childId, child.getLevel());
		}
		log.info("Succcesfully added Node to Graph");
		return new Response();
		
	}

	@Override
	public Node createNode(Long id, String nodeName, Level level, Float value) {
		log.info("Creating Node for id {}", id);
		Node node = Tree.INVESTOR.getNodeById(id);
		if (Objects.isNull(node)) {
			node = new Node(id, nodeName, level, value);
			Tree.INVESTOR.addNode(node);
		}
		log.info("Succcesfully created Node");
		return node;
	}

	private void addNodeToGraph(Long parentId, Long childId, Level level) {
		Map<Long, List<Long>> graph = updateNodesToGraph(parentId, childId, null);
		addEdge(graph, parentId, childId, level);
	}

	private void addNodeToGraph(Long parentId, Long childId, Integer weight, Level level) {
		Map<Long, List<Long>> graph = updateNodesToGraph(parentId, childId, weight);
		for (int i = 0; i < weight; i++) {
			addEdge(graph, parentId, childId, level);
		}
	}

	private Map<Long, List<Long>> updateNodesToGraph(Long parentId, Long childId, Integer weight) {
		Map<Long, List<Long>> graph = INVESTOR.getContext();
		if (!Objects.isNull(weight) && weight <= 0) {
			throw new InvalidInputException("Weight attribute cannot be less than 1");
		}
		graph.putIfAbsent(parentId, new ArrayList<>());
		graph.putIfAbsent(childId, new ArrayList<>());
		return graph;
	}

	private void addEdge(Map<Long, List<Long>> graph, long parentId, long childId, Level level) {
		// multiple entries for same childIds only allowed for level 3.
		if (level == Level.LEVEL_3 || !graph.get(parentId).contains(childId)) {
			graph.get(parentId).add(childId);
		}
	}

}
