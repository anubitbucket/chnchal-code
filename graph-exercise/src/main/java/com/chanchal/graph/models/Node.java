package com.chanchal.graph.models;

import com.chanchal.graph.enums.Level;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Node {
	private long id;
	private String name;
	private Level level;
	private Float value;

	public Node() {
	}

	public Node(long id, String name, Level level) {
		super();
		this.id = id;
		this.name = name;
		this.level = level;
	}

	public Node(long id, String name, Level level, Float value) {
		super();
		this.id = id;
		this.name = name;
		this.level = level;
		this.value = value;
	}

}
