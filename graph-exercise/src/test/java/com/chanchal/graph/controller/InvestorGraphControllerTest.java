package com.chanchal.graph.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.chanchal.graph.enums.Level;
import com.chanchal.graph.models.Node;
import com.chanchal.graph.service.GraphReader;
import com.chanchal.graph.service.GraphWriter;
import com.chanchal.graph.to.AddNodeRequest;
import com.chanchal.graph.to.Response;
import com.chanchal.graph.validator.RequestValidator;

@RunWith(MockitoJUnitRunner.class)
public class InvestorGraphControllerTest {

	private static final long ID_1 = 1L;

	private static final String NAME = "NAME";

	@InjectMocks
	private InvestorGraphController controller;

	@Mock
	private GraphReader graphReader;

	@Mock
	private GraphWriter graphWriter;

	@Mock
	private RequestValidator requestValidator;

	@Test
	public void testCreateNode() {
		Node node = new Node(1L, NAME, Level.LEVEL_1);
		when(graphWriter.createNode(eq(1L), eq(NAME), eq(Level.LEVEL_1), nullable(Float.class)))
				.thenReturn(node);
		Node createdNode = controller.createNode(node);
		assertNotNull(createdNode);
		assertEquals(1L, createdNode.getId());
		assertEquals(NAME, createdNode.getName());
		assertEquals(Level.LEVEL_1, createdNode.getLevel());
	}

	@Test
	public void testAddNode() {
		AddNodeRequest request = new AddNodeRequest();
		request.setChildId(2L);
		request.setParentId(ID_1);
		when(graphWriter.addNode(eq(1L), eq(2L), nullable(Level.class), nullable(Integer.class)))
				.thenReturn(new Response());
		Response response = controller.addNodeToGraph(request);
		assertNotNull(response);
		assertEquals("Success", response.getStatus());
	}
	
	@Test
	public void testGetMarketValue() {
		List<Long> list = Arrays.asList(2L);
		double expected = 10.0;
		when(graphReader.getMarketValue(eq(ID_1), eq(list))).thenReturn(expected);
		Double actual = controller.getMarketValue(ID_1, list);
		assertEquals(expected, actual);
	}

}
