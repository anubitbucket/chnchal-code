package com.chanchal.graph.service.impl;

import static com.chanchal.graph.enums.Graph.INVESTOR;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.chanchal.graph.enums.Level;
import com.chanchal.graph.enums.Tree;
import com.chanchal.graph.models.Node;
import com.chanchal.graph.service.impl.GraphReaderImpl;

@RunWith(MockitoJUnitRunner.class)
public class GraphReaderImplTest {

	@InjectMocks
	private GraphReaderImpl service;
	
	@Test
	public void testGetMarketValueForLevel3() {
		INVESTOR.getContext().put(1L, Arrays.asList(1l));
		Float value = 10.0f;
		Tree.INVESTOR.addNode(new Node(10L, "name", Level.LEVEL_3, 10.0f));
		double actual = service.getMarketValue(10l, Collections.emptyList());
		assertEquals(Double.valueOf(value), actual);
	}

	@Test
	public void testGetMarketValueForLevel1() {
		INVESTOR.getContext().put(1L, Arrays.asList(2l));
		INVESTOR.getContext().put(2L, Arrays.asList(3l));
		INVESTOR.getContext().put(3L, Collections.emptyList());
		Float value = 10.0f;
		Tree.INVESTOR.addNode(new Node(1L, "name", Level.LEVEL_1));
		Tree.INVESTOR.addNode(new Node(2L, "name", Level.LEVEL_2));
		Tree.INVESTOR.addNode(new Node(3L, "name", Level.LEVEL_3, value));
		double actual = service.getMarketValue(1L, Collections.emptyList());
		assertEquals(Double.valueOf(value), actual);
	}
	
	@Test
	public void testGetMarketValueForLevel2() {
		INVESTOR.getContext().put(1L, Arrays.asList(2l));
		INVESTOR.getContext().put(2L, Arrays.asList(3l));
		INVESTOR.getContext().put(3L, Collections.emptyList());
		Float value = 10.0f;
		Tree.INVESTOR.addNode(new Node(1L, "name", Level.LEVEL_1));
		Tree.INVESTOR.addNode(new Node(2L, "name", Level.LEVEL_2));
		Tree.INVESTOR.addNode(new Node(3L, "name", Level.LEVEL_3, value));
		double actual = service.getMarketValue(2L, Collections.emptyList());
		assertEquals(Double.valueOf(value), actual);
	}

}
