package com.chanchal.graph.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.chanchal.graph.enums.Level;
import com.chanchal.graph.enums.Tree;
import com.chanchal.graph.exceptions.InvalidInputException;
import com.chanchal.graph.models.Node;
import com.chanchal.graph.service.impl.GraphWriterImpl;
import com.chanchal.graph.to.Response;
import com.chanchal.graph.validator.RequestValidator;

@RunWith(MockitoJUnitRunner.class)
public class GraphWriterImplTest {

	@InjectMocks
	private GraphWriterImpl service;
	@Mock
	private RequestValidator requestValidator;

	@Test
	public void testAddNode() {
		Tree.INVESTOR.addNode(new Node(1L, "name", Level.LEVEL_1));
		Tree.INVESTOR.addNode(new Node(2L, "name", Level.LEVEL_2));
		Tree.INVESTOR.addNode(new Node(3L, "name", Level.LEVEL_3, 10.0f));
		Response response = service.addNode(2l, 3l, Level.LEVEL_3, 1);
		assertEquals("Success", response.getStatus());
	}

	@Test(expected = InvalidInputException.class)
	public void testAddNodeThrowsException() {
		Tree.INVESTOR.addNode(new Node(1L, "name", Level.LEVEL_1));
		Tree.INVESTOR.addNode(new Node(2L, "name", Level.LEVEL_2));
		Tree.INVESTOR.addNode(new Node(3L, "name", Level.LEVEL_3, 10.0f));
		service.addNode(2l, 3l, Level.LEVEL_3, 0);
	}

	@Test
	public void testAddNodeWhenParentNodesDontExist() {
		Response response = service.addNode(1l, 2l, Level.LEVEL_1, null);
		assertEquals("Success", response.getStatus());
	}
}
