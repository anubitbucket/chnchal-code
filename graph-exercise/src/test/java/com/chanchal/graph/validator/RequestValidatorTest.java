package com.chanchal.graph.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.chanchal.graph.enums.Level;
import com.chanchal.graph.exceptions.InvalidInputException;
import com.chanchal.graph.models.Node;
import com.chanchal.graph.to.AddNodeRequest;

@RunWith(MockitoJUnitRunner.class)
public class RequestValidatorTest {

	@InjectMocks
	private RequestValidator validator;

	@Test
	public void testValidateAddNodeRequest() {
		AddNodeRequest request = new AddNodeRequest();
		request.setChildId(2l);
		request.setLevel(Level.LEVEL_1);
		request.setParentId(1L);
		validator.validateAddNodeRequest(request);
	}

	@Test(expected = InvalidInputException.class)
	public void testValidateAddNodeRequestForInvalidChildId() {
		AddNodeRequest request = new AddNodeRequest();
		request.setChildId(null);
		request.setLevel(Level.LEVEL_1);
		request.setParentId(1L);
		validator.validateAddNodeRequest(request);
	}

	@Test(expected = InvalidInputException.class)
	public void testValidateAddNodeRequestForInvalidParentId() {
		AddNodeRequest request = new AddNodeRequest();
		request.setChildId(2l);
		request.setLevel(Level.LEVEL_1);
		request.setParentId(null);
		validator.validateAddNodeRequest(request);
	}
	
	@Test
	public void testValidateCreateNodeRequest() {
		Node request = new Node(1L, "Name", Level.LEVEL_1);
		validator.validateCreateNodeRequest(request );
	}
	
	@Test(expected = InvalidInputException.class)
	public void testValidateCreateNodeRequestForInvalidId() {
		Node request = new Node(0l, "Name", Level.LEVEL_1);
		validator.validateCreateNodeRequest(request );
	}
	
	@Test(expected = InvalidInputException.class)
	public void testValidateCreateNodeRequestForInvalidLevel() {
		Node request = new Node(1l, "Name", null);
		validator.validateCreateNodeRequest(request );
	}
	
	@Test(expected = InvalidInputException.class)
	public void testValidateCreateNodeRequestForInvalidName() {
		Node request = new Node(1l, "", Level.LEVEL_1);
		validator.validateCreateNodeRequest(request );
	}
	
	@Test
	public void testValidateLevelsForParentChild() {
		validator.validateLevelsForParentChild(Level.LEVEL_1, Level.LEVEL_2);
	}
	
	@Test(expected = InvalidInputException.class)
	public void testValidateLevelsForParentChildThrowsException() {
		validator.validateLevelsForParentChild(Level.LEVEL_3, Level.LEVEL_2);
	}
	
	@Test
	public void testvalidateWeightForLevel3Child() {
		validator.validateWeightForLevel3Child(Level.LEVEL_3, 1);
	}
	
	@Test(expected = InvalidInputException.class)
	public void testvalidateWeightForLevel3ChildThrowsException() {
		validator.validateWeightForLevel3Child(Level.LEVEL_3, 0);
	}
}
